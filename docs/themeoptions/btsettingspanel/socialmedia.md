The **Social Media tab** allows for social media links and logos to be added to your SharePoint Social Zone.

![social-media-tab.png](../../images/social-media-tab.png)

### Social Media

To add a Social Media link follow the steps bellow: 

1. Click on the plus (**+**) sign;

    **Note:** By default, when adding a new platform, Facebook will be selected. 
    
2. To change the social media platform, click on the logo;

3. A window prompting you to select a social media platform will appear;

    ![social-media-icon.png](../../images/social-media-icon.png)

4. Select the corresponding platform and confirm, by clicking on the **Select** button. 

5. After completing the above-mentioned steps, a new entry will be added; 

    ![social-media-platform.png](../../images/social-media-platform.png)

6. Add the URL that the icon should point to. 

    **Note:** The icon will only show after setting up it's corresponding URL.

7. The added links will be visible on your theme's social zone.

    ![social-media-links.png](../../images/social-media-links.png)

You're done ✅

### Social Media Color 

![social-media-color.png](../../images/social-media-color.png)

The Social Media Color allows you to change the color of your icons, based on the color scheme of the BindTuning theme currently installed.   


