The **Navigation tab** will allow you to set up and fine tune your navigation.

![bindtuning-settings-navigation.png](../../images/bindtuning-settings-navigation.png)


### Manage

#### Menu Provider
- <b>Dynamic</b>: Uses SharePoint's default navigation;
- <b>Global</b>: Uses SharePoint's global navigation;
- <b>Current</b>: Uses SharePoint's current navigation;
- <b>Term set</b>: Allows you to use a **custom term set** as your site navigation;
- <b>Legacy</b>: Uses the old navigation provider (BindMENU options unavailable);
- <b>None</b>: Removes any navigation from your site.

---
#### Term Set ID
If the **Menu Provider** chosen is **Term Set**, this option will automatically appear.
This option takes in the ID of your term set, found in **SharePoint’s term store management**;

---
#### Pull navigation from

Allows to pull navigation elements from either the current site collection or from another SharePoint site. 

---
#### Use managed navigation if set
If enabled, this option will allow you to represent your navigation elements using a Managed Metadata term set.

---
#### Enable accessibility features

If enabled, this option will allow you turn on accessibility features for the navigation, allowing users to quickly navigate your SharePoint site using keyboard controls. <br>
Further information can be found <a href="https://support.bindtuning.com/hc/en-us/articles/360030533692" target="_blank">here</a>.

---
### Look and Feel

#### Hide Recent items
Hides the recent items menu item. 

**Note:** This option is only available for Modern SharePoint Team sites.

---
#### Hide Recycle bin
Hides the recycle bin menu item.

**Note:** This option is only available for Modern SharePoint Team sites.

---
#### Sub-menu style

The sub-menu style will allow you to change between two distinct functional and presentational styles:

- <b>Default</b>: Corresponds to the normal sub-menu hierarchy, presenting one level at a time;
- <b>Mega Menu</b>: Displays the entire hierarchy of menu items in one big sub-menu.

---
#### Type of animation
- <b>Linear</b>: The fade-in animaton occurs at a constant pace;
- <b>Ease</b>: The fade-in animation starts slowly, proceeds to accelerate, and slows down again;
- <b>Ease In</b>: The fade in animation starts slowly then accelerates;
- <b>Ease Out</b>: The fade in animation starts quickly and slows down near the end.

---

#### Sub-menu fade in speed
- <b>Fast</b>: The sub-menu takes 200ms to fade in;
- <b>Normal</b>: The sub-menu takes 300ms to fade in;
- <b>Slow</b>: The sub-menu takes 400ms to fade in.

---

#### Sub-menu fades-in from

Allows you to change the direction under which the sub-menu fade in occurs from: 

- <b>Top</b>;
- <b>Right</b>;
- <b>Bottom</b>;
- <b>Left</b>.

---

#### Maximum and minimum sub-menu width
Set in pixels, you can use the slider, or the input to right of the setting label, to apply restrictions on the maximum and minimum width for the sub-menu.

---
#### Max number of horizontal menu items
Sets the maximum number of menu items, hiding the remaining ones in a sub-menu.