
#### Links

The **link zones** built into the theme allow you to add custom links to the header and footer. To do that, simply follow the below-mentioned steps:

![usinglinks.png](../images/usinglinks.png)

1. Click **Edit** on the top right corner of the page content - this will put the page on edit mode;

	**Note:** Alternatively, if you’re on a library page, you can open up your browser’s inspector by pressing F12, selecting the **Console** tab, and typing **btEditMode()**.</p>

2. The header and footer will reveal editable content areas. 

3. Hover the section you wish to edit and click the **Edit** button ✏️;

4. Add your custom link and title and click **Save**;

5. Proceed to **Publish** your page.

	**Note:** Alternatively, if you’re on a library page, you can open up your browser’s inspector by pressing F12, selecting the **Console** tab, and typing **btEditModeSave()**.</p>

You’re done! ✅

---
#### Content Zones

Additionaly, BindTuning themes come equipped with the ability to add and customize content zones. The **content zones**, built into the theme, allow you to add custom ```html``` to the header and footer through the following steps:

![contentzones.png](../images/contentzones.png)

1. Click **Edit** on the top right corner of the page content;

	**Note:** Alternatively, if you’re on a library page, you can open up your browser’s inspector by pressing F12, selecting the **Console** tab, and typing **btEditMode()**.</p>

2. This will put the page on edit mode. The header and footer will reveal editable content areas. 	

3. Hover the section you wish to edit and click the **Edit** button ✏️;
4. Add your custom HTML and click **Save** and then **Publish**;

	**Note:** Alternatively, if you’re on a library page, you can open up your browser’s inspector by pressing F12, selecting the **Console** tab, and typing **btEditModeSave()**.</p>

You’re done! ✅