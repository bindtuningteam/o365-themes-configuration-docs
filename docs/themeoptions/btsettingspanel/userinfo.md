By clicking on your gravatar at the bottom left of the **BindTuning Settings Panel**, you'll be able to access relevant information for both your account, usage and configuration.

![panel-info.png](../../images/panel-info.png)


#### Feedback

This option will redirect you to our <a href="https://bindtuning.uservoice.com/forums/10329-my-bindtuning-idea">User Voice</a>. There, you'll be able to leave any comment and ideas on how we can improve our products.

---
#### Documentation

By clicking on this item, you'll be redirected to our theme installation <a href="https://bindtuning-office-365-themes.readthedocs.io/en/latest/modern/requirements/">user guides</a>. 

---
#### Change log

This option will allow you to view any recent modification and/or correction made to any <a href="https://support.bindtuning.com/hc/en-us/sections/201001919-Announcements">new release</a> of BindTuning's themes. 

---
#### License

This option will redirect you to our <a href="https://bindtuning-legal.readthedocs.io/en/latest/Licenses/BindTuning%20End-User%20License%20Agreement/">End-User License Agreement</a>.

---
#### My account

By clicking on this option, you'll be redirected to your BindTuning account.

---
#### Theme Info

The theme information section will allow you to view theme-specific information, namely: 

- <b>Theme name</b>; 
- <b>Theme version</b>; 
- <b>Theme pack</b>.

---
### Dark Mode

![settings-darkmode.png](../../images/settings-darkmode.png)


To toggle BindTuning's settings panel dark mode, simply click on the moon icon. 
