The General's tab offers more granular control on the look and feel of your BindTuning theme.

![bindtuning-settings-general.png](../../images/bindtuning-settings-general.png)

### General

#### Favicon

Sets a custom Favicon for your site collection. 

<p class="alert alert-info"><b>Not sure what a Favicon is?</b> <br>
Favicon is an icon that is picked up from the website to accompany the site name in the browser tabs and the Bookmarks/Favorites bar.</p>

![16-favicon.png](../../images/16.favicon.png)

___
#### Logo image URL

Allows to change the default SharePoint image. To do so, simply point to the URL where the image, you want to set as the icon, is stored.

---

#### Logo Link URL

Sets a custom URL for your logo to link to. Leave empty if you don't want your logo to link anywhere.

Can also be used tokens to get the relative URL of the:<br>
- current site: **{site}** - The use of this token can be useful when,the theme is applied to a site collection with sub-sites, the logo will be linked to each current site.<br>
- site collection: **{sitecollection}** - The use of this token can be useful when, the theme is applied to a site collection and his sub-sites and each one has the logo will be linked to parent site collection.<br><br>

The use of both tokens can be useful when, the theme is applied to an Hub and associated sites and each one has to have the logo linked to the current site collection.

---

#### Page title link URL 

Sets a custom link on the page title. Leave empty if you don't want to display said link.

Can also be used tokens to get the relative URL of the:<br>
- current site: **{site}** - The use of this token can be useful when,the theme is applied to a site collection with sub-sites, the page title will be linked to each current site.<br>
- site collection: **{sitecollection}** - The use of this token can be useful when, the theme is applied to a site collection and his sub-sites and each one has the page title will be linked to parent site collection.<br><br>

The use of both tokens can be useful when, the theme is applied to an Hub and associated sites and each one has to have the page title linked to the current site collection.

---
#### Logo's maximum width and height

Allows to adjust the concrete dimensions for your custom logo image.

### Look and Feel

#### Header and Footer style

The compact styling option will reduce the spacing between elements like the logo and navbar, paddings in the footer and header, etc. The compact styling will, subsequently, increase the available content area.

---
#### Shy Header

On Modern SharePoint sites, when scrolling, you'll verify the existance of a new navigation element called Shy Header. This option will allow you to enable or disable that same element.

---
#### Heading fonts

This option will allow you to define whether the theme heading typefaces, defined when customizing the theme, will be used by either the theme and correlative web parts and content or just the elements relative to the theme.

---
#### Hide native hub navigation

When enabled, this option hides the native hub site navigation, created by default. 

**Note:** This option will only show up for themes applied in a Hub Site.

---
#### Hub navigation position
Allows to define whether the hub navigation is displayed before or after the theme header.

**Note:** This option will only show up for themes applied in a Hub Site.

---
#### Hide native search

This option allows to hide SharePoint's native search bar. 

---
#### Hide sidebar

Enabling this option will hide the sidebar, displayed, by default, when on a Modern Team Site.

---
#### Hide Footer

This option allows to hide the footer, increasing the available content area. 

---
#### Hide Breadcrumb

When enabled, this option will allow you to hide the breadcrumbs from your SharePoint sites.

---
#### Hide Page Title

This option will allow you to hide your SharePoint's page title.




