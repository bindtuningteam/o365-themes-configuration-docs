The **Actions tab** allows to show or hide the tokens associated with your SharePoint site.

![bindtuning-settings-actions.png](../../images/bindtuning-settings-actions.png)

---
#### SharePoint Actions

This option allows to choose which elements to display, when on a site collection where a BindTuning theme has been applied:

- <b>Create site</b>;
- <b>Members</b>;
- <b>Share site</b>;
- <b>Follow site</b>;
- <b>Next steps</b>.


