The **Search tab** will allow you to modify searchbar-related settings.

![bindtuning-settings-search.png](../../images/bindtuning-settings-search.png)

### Manage

#### Show search results on

Allows you to define how you want to display the contents returned by the search query. 
The search results can be displayed in two different formats:

- <b>Search page</b>;
- <b>Popup</b>.

---
#### Search results scope

This option allows to select the scope returned by any search query. The scope can be set for: 

- <b>Local</b>: Returning results for the specific site collection; 
- <b>Global</b>: Returning results for the entirety of the site collections belonging to the particular tenant.

---
#### Search Center URL 

This option allows to redirect the results of your SharePoint search query to a Search Center site collection.

---
### Look and Feel

#### Layout

Allows to adjust the layout of the searchbar. This option can be set to:

- <b>Full</b>;
- <b>Compact</b>;

---
#### Placeholder text

Customize the text of the search placeholder.

---
#### Maximum search result items
This option allows to set maximum number of search result items to be shown inside the search popup.