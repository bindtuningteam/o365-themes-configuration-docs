Despite offering great advantages in terms of configuration and ease of use, the Modern Experience, for both Office 365 and SharePoint 2019, removed various customization options available for the Classic Experience, found in older SharePoint versions.
Due to the limited customization options available out-of-the-box, BindTuning themes provide you with the flexibility to further enhance your intranet identity, by providing a set of extra functionalities, fully available, after installing your theme. 
<br>
The features present are fully functional and build on top of the **look and feel** customizations made while selecting and configuring your theme. <p>

Note that this section refers to the **post-deployment configuration** of a BindTuning theme. <br> 
Instructions on how to install BindTuning themes can be found <a href="https://bindtuning-office-365-themes.readthedocs.io/en/latest/">here</a>.

___
These features can be accessed through our **BindTuning Settings Panel**. 
To access the panel, follow the instructions below: 

1. On the command bar, click on **BindTuning Settings** option;

![bindtuning-settings-button.png](../images/bindtuning-settings-button.png)

**Note:** Alternatively you can open up your browser’s inspector by pressing F12 and on the console and typing **btOpenSettings()**.

2. A settings panel will open from the right. The panel is divided into 7 sections:

    - <a href="https://bindtuning-office-365-themes-configuration.readthedocs.io/en/latest/themeoptions/btsettingspanel/general/">General</a>;
    - <a href="https://bindtuning-office-365-themes-configuration.readthedocs.io/en/latest/themeoptions/btsettingspanel/navigation/">Navigation</a>;
    - <a href="https://bindtuning-office-365-themes-configuration.readthedocs.io/en/latest/themeoptions/btsettingspanel/search/">Search</a>;
    - <a href="https://bindtuning-office-365-themes-configuration.readthedocs.io/en/latest/themeoptions/btsettingspanel/actions/">Actions</a>;
    - <a href="https://bindtuning-office-365-themes-configuration.readthedocs.io/en/latest/themeoptions/btsettingspanel/socialmedia">Social Media</a>;
    - <a href="https://bindtuning-office-365-themes-configuration.readthedocs.io/en/latest/themeoptions/btsettingspanel/developer/">Developer</a>;
    - <a href="https://bindtuning-office-365-themes-configuration.readthedocs.io/en/latest/themeoptions/btsettingspanel/advanced/">Advanced</a>;
    - <a href="https://bindtuning-office-365-themes-configuration.readthedocs.io/en/latest/themeoptions/btsettingspanel/userinfo/">User Information</a>.