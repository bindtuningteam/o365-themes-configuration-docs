### Platform version
- Modern Office 365 (Modern SharePoint Online)

### Browsers

BindTuning Themes work in all modern browsers:

- Firefox
- Chrome
- Safari
- Opera
- Edge
- IE
